Vue.component('my-component', {
	template: '#hello-world-template',
	props: ['my-prop'],
	data() {
		return {
			adios: "fsd"


		}
	},
  computed: {

	}



})
new Vue({
	el: '#caracolVue',

	data: {
		items: ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j']
	},

	computed: {
		phi() {
			return (1 + Math.sqrt(5))/2;
		},
		sizes() {
			var sizes = [];
			for (var n = 0; n < this.items.length; n++) {
				var width = n == 0 ? 630 : (this.phi - 1)*sizes[n-1].width

				var size = {
					width:width,
					height:this.phi*width,
				}
				sizes[n] = size;

		}
		return sizes;
	},

	mounted() {

		}
	}
});
